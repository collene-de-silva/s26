// What directive is used by Node.js in loading the modules it needs?
	//require() directive


// What Node.js module contains a method for server creation?
	//HTTP module


// What is the method of the http object responsible for creating a server using Node.js?
	//createServer() method


// What method of the response object allows us to set status codes and content types?
	//writeHead() method


// Where will console.log() output its contents when run in Node.js?
	//At the terminal window



// What property of the request object contains the address's endpoint?
	//the url property